
#include <Adafruit_Fingerprint.h>
#include <SoftwareSerial.h>
#include <ArduinoJson.h>

//PINES DIGITALES
const int RX_BLUETOOTH = 6;
const int TX_BLUETOOTH = 7;

const int RELAY_DOOR = 4;
const int RELAY_ELECTRICITY = 5;

//Arduino mode
const int DOOR_MODE = 1;
const int ELECTRICITY_MODE = 2;

const int SWITCH = 2;

SoftwareSerial bluetoothSerial(RX_BLUETOOTH, TX_BLUETOOTH);

int currentFingerprintMode;
int currentArduinoMode;

Adafruit_Fingerprint finger = Adafruit_Fingerprint(&Serial);

StaticJsonBuffer<100> jsonStringBuffer;

void setupFingerprint(){
  bluetoothSerial.println("Configuring fingerprint sensor enrollment");
  finger.begin(57600);
  if (finger.verifyPassword()) {
    bluetoothSerial.println("Found fingerprint sensor!");
  } else {
    bluetoothSerial.println("Did not find fingerprint sensor :(");
    while (1);
  }
}

void setupBlueToothConnection() {
  pinMode(RX_BLUETOOTH, INPUT);
  pinMode(TX_BLUETOOTH, OUTPUT);
  bluetoothSerial.begin(38400);                           // Set BluetoothBee BaudRate to default baud rate 38400
  bluetoothSerial.print("\r\n+STWMOD=0\r\n");             // set the bluetooth work in slave mode
  bluetoothSerial.print("\r\n+STNA=BTSC\r\n");    // set the bluetooth name as "BTSC"
  bluetoothSerial.print("\r\n+STOAUT=1\r\n");             // Permit Paired device to connect me
  bluetoothSerial.print("\r\n+STAUTO=0\r\n");             // Auto-connection should be forbidden here
  delay(2000);                                            // This delay is required.
  bluetoothSerial.print("\r\n+INQ=1\r\n");                // make the slave bluetooth inquirable
  //Serial.println("The slave bluetooth is inquirable!");
  delay(2000);                                            // This delay is required.
  bluetoothSerial.flush();
}

void setup(){
  //Serial.begin(9600);
  pinMode(RELAY_DOOR, OUTPUT);
  pinMode(RELAY_ELECTRICITY, OUTPUT);
  pinMode(SWITCH, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(SWITCH), verifyArduinoMode, CHANGE);
  //finger.emptyDatabase();
  setupBlueToothConnection();
  setupFingerprint();
  verifyArduinoMode();
  closeDoor();
  lockElectricityFlow();
}

void loop(){
  listenBluetoothCommand();
  identifyUser();
  delay(50);
}

void verifyArduinoMode(){
  int r = digitalRead(SWITCH);
  //Serial.println(r);
  if(r == LOW){
    if(currentArduinoMode != DOOR_MODE){
      currentArduinoMode = DOOR_MODE;
      bluetoothSerial.println("DOOR_MODE");
    }
  }else{
    if(currentArduinoMode != ELECTRICITY_MODE){
      currentArduinoMode = ELECTRICITY_MODE;
      bluetoothSerial.println("ELECTRICITY_MODE");
    }
  }
}

void listenBluetoothCommand(){
  String jsonString = "";
  while(bluetoothSerial.available() > 0){
    jsonString += (char)bluetoothSerial.read();
  }
  if(jsonString != ""){
    bluetoothSerial.println(jsonString);
    JsonObject& root = jsonStringBuffer.parseObject(jsonString);
    String command = root["command"];
    bluetoothSerial.println(command);
    if(command == "eu"){
      int id = enrollUser();
      if(id == -1){
        bluetoothSerial.println("{'type':'res','res':'error','action':'eu'}");
        return;
      }
      bluetoothSerial.println("{'type':'res','res':'ok','action':'eu'}");
    }else if(command == "du") {
      int userId = root["userId"];
      if(!deleteUser(userId)){
        bluetoothSerial.println("{'type':'res','res':'error','action':'du'}");
      }
      bluetoothSerial.println("{'type':'res','res':'ok','action':'du'}");
    }else if(command == "od") {
      openDoor();
      bluetoothSerial.println("{'type':'res','res':'ok','action':'od'}");
    }else if(command == "cd") {
      closeDoor();
      bluetoothSerial.println("{'type':'res','res':'ok','action':'cd'}");
    }else if(command == "lef") {
      lockElectricityFlow();
      bluetoothSerial.println("{'type':'res','res':'ok','action':'lef'}");
    }else if(command == "uef") {
      unlockElectricityFlow();
      bluetoothSerial.println("{'type':'res','res':'ok','action':'uef'}");
    }
  }
}

void identifyUser(){
  uint8_t p = finger.getImage();
  if (p != FINGERPRINT_OK)  return;

  p = finger.image2Tz();
  if (p != FINGERPRINT_OK)  return;

  p = finger.fingerFastSearch();
  if (p != FINGERPRINT_OK)  return;

  // found a match!
  bluetoothSerial.print("Found ID #");
  bluetoothSerial.print(finger.fingerID);
  bluetoothSerial.print(" with confidence of ");
  bluetoothSerial.println(finger.confidence);
  if(currentArduinoMode == DOOR_MODE){
    bluetoothSerial.println("Opening door");
  }else{
    bluetoothSerial.println("Opening electricity flow");
  }
  //return finger.fingerID;
}

bool takeFingerprintCapture(int number){
  int p = -1;
  bluetoothSerial.println("{'type':'msg', 'msg':'waiting_fingerprint'}");
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    if(p != FINGERPRINT_OK && p != FINGERPRINT_NOFINGER){
      bluetoothSerial.println("{'type':'msg', 'msg':'error_scaning_fingerprint'}");
    }
  }
  // OK success!
  p = finger.image2Tz(number);
  if(p != FINGERPRINT_OK){
    bluetoothSerial.println("{'type':'msg', 'msg':'failed_fingerprint_capture'}");
    return false;
  }
  bluetoothSerial.println("{'type':'msg', 'msg':'success_fingerprint_capture'}");
  return true;;
}

bool createFingerprintModel(){
  int p = finger.createModel();
  if (p != FINGERPRINT_OK) {
    bluetoothSerial.println("{'type':'msg', 'msg':'failed_create_fingerprint_model'}");
    return false;
  }
  return true;
}

bool storeFingerprintModel(int id){
  int p = finger.storeModel(id);
  if (p != FINGERPRINT_OK) {
    bluetoothSerial.println("{'type':'msg', 'msg':'failed_store_fingerprint_model'}");
    return false;
  }
  return true;
}

int enrollUser(){
  finger.getTemplateCount();
  int id = finger.templateCount + 1;
  if(!takeFingerprintCapture(1)){
    return -1;
  }
  bluetoothSerial.println("{'type':'msg', 'msg':'place_same_finger_again'}");
  delay(2000);
  if(!takeFingerprintCapture(2)){
    return -1;
  }
  if(!createFingerprintModel()){
    return -1;
  }
  if(!storeFingerprintModel(id)){
    return -1;
  }
  return id;
}

bool deleteUser(uint8_t id){
  int p = -1;
  p = finger.deleteModel(id);
  if (p != FINGERPRINT_OK) {
    return false;
  }
  return true;
}

void openDoor(){
  digitalWrite(RELAY_DOOR, LOW);
}

void closeDoor(){
  digitalWrite(RELAY_DOOR, HIGH);
}

void lockElectricityFlow(){
  digitalWrite(RELAY_ELECTRICITY, HIGH);
}

void unlockElectricityFlow(){
  digitalWrite(RELAY_ELECTRICITY, LOW);
}
